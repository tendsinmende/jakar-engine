use core::next_tree::JakarNode;
use core::engine_settings;
use render::render_helper;
use render::frame_system::FrameSystem;
use render::pipeline_manager::PipelineManager;
use render::renderer::RenderDebug;
use core::resource_management::asset_manager::AssetManager;
use core::next_tree::{SceneTree, ValueTypeBool, SceneComparer};
use core::next_tree::content::ContentType;
use core::resources::camera::Camera;
use render::uniform_manager::UniformManager;
use render::renderer::JkCommandBuff;

use vulkano::command_buffer::AutoCommandBufferBuilder;

use std::sync::{Arc, RwLock};

use jakar_threadpool::*;

///Collects all thingy which are needed to render all objects in the forward pass
pub struct ObjectSystem {
    engine_settings:  Arc<RwLock<engine_settings::EngineSettings>>,

    ///reference to the pipeline manager, needed to draw debug information. Will be changed maybe.
    pipeline_manager: Arc<RwLock<PipelineManager>>,
    uniform_manager: Arc<RwLock<UniformManager>>,

}


impl ObjectSystem{
    pub fn new(
        engine_settings:  Arc<RwLock<engine_settings::EngineSettings>>,
        pipeline_manager: Arc<RwLock<PipelineManager>>,
        uniform_manager: Arc<RwLock<UniformManager>>,
    ) -> Self{

        ObjectSystem{
            engine_settings,
            pipeline_manager,
            uniform_manager,
        }
    }

    ///renders several forward shadeable nodes in this asset managers active scene.
    ///Returns the CommandBuffer passless
    pub fn do_forward_shading(
        &mut self,
        frame_system: &FrameSystem,
        asset_manager: &mut AssetManager,
        command_buffer: &mut JkCommandBuff,
        thread_pool: &mut ThreadPool,
        debug: &mut RenderDebug,
    ){

        debug.start_node_getting();

        let mesh_comparer = SceneComparer::new()
        .with_value_type(ValueTypeBool::none().with_mesh())
        .with_frustum(asset_manager.get_camera().get_frustum_bound())
        .with_cull_distance(0.1, asset_manager.get_camera().get_view_projection_matrix())
        .without_transparency();


        //now we can actually start the frame
        //get all opaque meshes
        let opaque_meshes = asset_manager
        .get_active_scene()
        .copy_all_nodes(&Some(mesh_comparer));

        //Get us the current graphics command buffer
        let mut command_buf = command_buffer.get_graphics_buffer();


        //TODO reimplement transparency
        /*
        let mesh_comp_trans = mesh_comparer.clone()
        .with_transparency();
        //get all translucent meshes
        let translucent_meshes = asset_manager
        .get_active_scene()
        .copy_all_nodes(&Some(mesh_comp_trans));
        //now send the translucent meshes to another thread for ordering

        let trans_recv = render_helper::order_by_distance(
            translucent_meshes,
            asset_manager.get_camera(),
            thread_pool
        );
        */
        debug.end_node_getting();
        //=========================================================================================
        //Go into the forward shading stage for opaque objects
        //first get the framebuffer for the forward pass
        let forward_frame_buffer = frame_system.get_passes().get_gbuffer_framebuff();

        //For successfull clearing we generate a vector for all images.
        let clearing_values = vec![
            [0.0, 0.0, 0.0, 0.0].into(), //albedo
            1f32.into(), //depth
            [1.0].into(), //roughness
            [1.0].into(), // metallic
            [0.0, 0.0, 1.0].into(), //normal
            [0.0, 0.0, 0.0, 0.0].into(), //pos
            [1.0].into(), //ao
        ];

        //Start forward pass
        let mut new_cb = command_buf.begin_render_pass(forward_frame_buffer, false, clearing_values)
            .expect("failed to start main renderpass");
        //Warp into an option to make ownership transfering easier

        //Draw opaque meshes
        //now we are in the main render pass in the forward pass, using this to draw all meshes
        //add all opaque meshes to the command buffer
        for opaque_mesh in opaque_meshes.iter(){
            let transform = opaque_mesh.get_attrib().get_matrix();
            if let ContentType::Mesh(ref mesh) = opaque_mesh.get_value(){
                debug.start_mesh_capture();
                let mesh_lck = mesh.read().expect("failed to lock mesh for drawing!");
                new_cb = mesh_lck.draw(
                    new_cb,
                    frame_system,
                    transform,
                    debug
                );
                debug.end_mesh_capture();
            }else{
                println!("Mesh was no actual mesh...", );
                continue;
            }
        }

        new_cb = self.draw_opaque_debug(
            new_cb,
            frame_system,
            asset_manager,
            &opaque_meshes
        );

        //finally end this pass and return.
        new_cb = new_cb.end_render_pass().expect("failed to end object pass");

        //Replace the graphics cb with this one
        command_buffer.set_graphics_buffer(new_cb);
    }

    ///Draws all opaque debug information, like bounds of meshes, lines from traces etc.
    pub fn draw_opaque_debug(
        &self,
        command_buffer: AutoCommandBufferBuilder,
        frame_system: &FrameSystem,
        asset_manager: &mut AssetManager,
        drawn_meshes: &Vec<JakarNode>,
    ) -> AutoCommandBufferBuilder{

        //TODO Read the bound drawing info from the scene tree if needed
        let draw_bounds = {
            let settings = self.engine_settings.read().expect("Failed to lock settings.");
            let render_settings = settings.get_render_settings();
            let draw_bounds =  render_settings.get_debug_settings().draw_bounds;
            draw_bounds
        };

        let mut worker_cb = command_buffer;
        //Draw the bounds of each node in the tree
        if draw_bounds{

            //TODO Actually draw BVH not mesh / node bounds
            worker_cb = asset_manager.get_mesh_bvh().draw_bounds(
                worker_cb,
                self.pipeline_manager.clone(),
                frame_system.get_device(),
                self.uniform_manager.clone(),
                frame_system.get_dynamic_state()
            );
/*
            //Also draw the mesh bvh bounds from before
            for opaque_mesh in drawn_meshes.iter(){
                let transform = opaque_mesh.get_attrib().get_matrix();
                if let ContentType::Mesh(ref mesh) = opaque_mesh.get_value(){
                    let mesh_lck = mesh.read().expect("failed to lock mesh for drawing!");
                    worker_cb = mesh_lck.draw_bvh(
                        worker_cb,
                        self.pipeline_manager.clone(),
                        frame_system.get_device(),
                        self.uniform_manager.clone(),
                        frame_system.get_dynamic_state(),
                        Some(transform)
                    );
                }else{
                    println!("Mesh was no actual mesh...", );
                    continue;
                }
            }
*/
            //TODO might draw the mesh bounds later again.
            /*

            let nodes = asset_manager.get_active_scene().copy_all_nodes(&None);

            for node in nodes.iter(){
                worker_cb = render_helper::add_bound_draw(
                    worker_cb,
                    self.pipeline_manager.clone(),
                    node,
                    frame_system.get_device(),
                    self.uniform_manager.clone(),
                    frame_system.get_dynamic_state()
                );
            }
            */
        }

        worker_cb
    }
}
