use vulkano::sampler::{Sampler, Filter, MipmapMode, SamplerAddressMode};
use vulkano::device::Device;
use vulkano::command_buffer::AutoCommandBufferBuilder;
use vulkano::buffer::BufferAccess;
use vulkano::buffer::immutable::ImmutableBuffer;
use vulkano::buffer::BufferUsage;
use vulkano::sync::GpuFuture;

use std::sync::Arc;

use render::renderer::JkCommandBuff;
use render::frame_system::FrameSystem;
use render::light_system::LightSystem;
use core::resource_management::asset_manager::AssetManager;

///Renders the diffuse image from the g buffer, generates multible new images.
pub mod diffuse;
///Handels shadow ray tracing / post progress
pub mod shadow;

///Collects all functionality which is use for the ray tracing stages in this engine
pub struct RayTracing {
    diffuse: diffuse::Diffuse,
    shadow: shadow::ShadowRayTracing,
}

impl RayTracing{
    pub fn new(device: Arc<Device>) -> Self{
        let g_buffer_sampler = Sampler::new(
            device.clone(),
            Filter::Linear, //nice blending between pixels if upscaling for some reason
            Filter::Linear,
            MipmapMode::Nearest, //Actually no mipmapping
            SamplerAddressMode::ClampToEdge,
            SamplerAddressMode::ClampToEdge,
            SamplerAddressMode::ClampToEdge,
            0.0, //mip_lod_bias
            1.0,
            0.0,
            0.0
        ).expect("Failed to create gbuffer sampler!");

        RayTracing{
            diffuse: diffuse::Diffuse::new(device.clone(), g_buffer_sampler.clone()),
            shadow: shadow::ShadowRayTracing::new(device.clone(), g_buffer_sampler.clone()),
        }
    }

    ///Adds the ray tracing execution to this frame. NOTE: the buffer future has to be executed before the
    ///Commandbuffer execution starts.
    pub fn execute_shadows(
        &mut self,
        command_buffer: &mut JkCommandBuff,
        framesystem: &FrameSystem,
        light_system: &LightSystem,
        asset_manager: &mut AssetManager
    ){
        //TODO do reflections

        //Generate the bvh buffer every of those stages needs
        //TODO test if Device local or immutable is better
        //asset_manager.get_mesh_bvh().print_tree();
        let usage = BufferUsage::all();

        let top_data = asset_manager.get_mesh_bvh().get_top_bvh();
        let (top_bvh_buffer, top_future) = ImmutableBuffer::from_iter(
            top_data.into_iter(),
            usage,
            framesystem.get_queue().compute //Upload on compute queue
        ).expect("failed to build bvh buffer for this frame");

        let bottom_bvh_data = asset_manager.get_mesh_bvh().get_bottom_bvh();
        let (bottom_bvh_buffer, bottom_future) = ImmutableBuffer::from_iter(
            bottom_bvh_data.into_iter(),
            usage,
            framesystem.get_queue().compute //Upload on compute queue
        ).expect("failed to build bvh buffer for this frame");

        let model_buffer_data = asset_manager.get_mesh_bvh().get_mesh_buffer();
        let (model_buffer, model_future) = ImmutableBuffer::from_iter(
            model_buffer_data.into_iter(),
            usage,
            framesystem.get_queue().compute //Upload on compute queue
        ).expect("failed to build bvh buffer for this frame");

        let face_buffer_data = asset_manager.get_mesh_bvh().get_face_buffer();
        let (face_buffer, face_future) = ImmutableBuffer::from_iter(
            face_buffer_data.into_iter(),
            usage,
            framesystem.get_queue().compute //Upload on compute queue
        ).expect("failed to build bvh buffer for this frame");

        //upload vertex buffer
        let vertex_buf_data = asset_manager.get_mesh_bvh().get_vertex_buffer();
        let (vertex_buffer, vert_future) = ImmutableBuffer::from_iter(
            vertex_buf_data.into_iter(),
            usage,
            framesystem.get_queue().compute //Upload on compute queue
        ).expect("failed to upload bvh vertex buffer");

        //Now join together all the future to have the point at which all bvh buffers are uploaded.
        let mut after_upload: Box<GpuFuture + Send + Sync> = Box::new(top_future.join(bottom_future));
        after_upload = Box::new(after_upload.join(model_future));
        after_upload = Box::new(after_upload.join(face_future));
        after_upload = Box::new(after_upload.join(vert_future));

        //Wait for the vertex to be uploaded
        command_buffer.join(after_upload);


        //DEBUG some ray tracing experimenting
        self.shadow.render_debug_shadow(
            command_buffer,
            framesystem,
            light_system,
            top_bvh_buffer,
            bottom_bvh_buffer,
            model_buffer,
            face_buffer,
            vertex_buffer
        );
    }

    ///Executes the diffuse shader which will shade the scene via the pbr model.
    pub fn shade_diffuse(
        &mut self,
        command_buffer: &mut JkCommandBuff,
        framesystem: &FrameSystem,
        light_system: &LightSystem,
        asset_manager: &mut AssetManager
    ){
        self.diffuse.shade_diffuse(
            command_buffer,
            framesystem,
            light_system,
            asset_manager
        );
    }

}
