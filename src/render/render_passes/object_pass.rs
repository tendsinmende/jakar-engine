use std::sync::Arc;
use vulkano::framebuffer::RenderPassAbstract;
use vulkano::device::Device;
use render::render_passes::UsedFormats;


#[derive(Clone)]
pub struct ObjectPass {
    pub render_pass: Arc<RenderPassAbstract + Send + Sync>
}

impl ObjectPass{
    pub fn new(
        device: Arc<Device>,
        formats: UsedFormats,
    ) -> Self{

        //Setup the render_pass layout for the forward pass
        let main_renderpass = Arc::new(
            ordered_passes_renderpass!(device.clone(),
                attachments: {
                    //albedo, inclding it's strength aka emission.
                    albedo: {
                        load: Clear,
                        store: Store,
                        format: formats.albedo,
                        samples: 1,
                    },

                    //scene depth for opaque objects
                    depth: {
                        load: Clear,
                        store: DontCare,
                        format: formats.depth,
                        samples: 1,
                    },
                    //Contains roughness values
                    roughness: {
                        load: Clear,
                        store: Store,
                        format: formats.roughness,
                        samples: 1,
                    },
                    //metallic values
                    metallic: {
                        load: Clear,
                        store: Store,
                        format: formats.metallness,
                        samples: 1,
                    },

                    //contains world normal
                    normal: {
                        load: Clear,
                        store: Store,
                        format: formats.normal,
                        samples: 1,
                    },
                    //The world position
                    pos: {
                        load: Clear,
                        store: Store,
                        format: formats.position,
                        samples: 1,
                    },
                    //Contains ao
                    ao: {
                        load: Clear,
                        store: Store,
                        format: formats.ambient,
                        samples: 1,
                    }
                },
                passes:[
                    {
                        color: [albedo, roughness, metallic, normal, pos, ao],
                        depth_stencil: {depth},
                        input: []
                    }
                ]

            ).expect("failed to create main render_pass")
        );
        ObjectPass{
            render_pass: main_renderpass
        }
    }
}
