use vulkano::command_buffer::DynamicState;
use render::uniform_manager::UniformManager;
use vulkano::device::Device;
use render::pipeline_manager::PipelineManager;
use vulkano::command_buffer::AutoCommandBufferBuilder;

use std::sync::{Arc, RwLock};
use std::f32::INFINITY;
use std::time::Instant;

use cgmath::*;
use collision::*;

use tools::vec_tools::SubVec;
use tools::math::time_tools::{dur_as_f32, as_ms};
use core::resources::mesh;
use render::shader::shader_inputs::ray_tracing;
use render::render_helper;

pub struct TimeDebug{
    find_index: Option<f32>,
    setup_node: Option<f32>,
    setup_leaf: Option<f32>,
    count_size: Option<f32>,
    clone: Option<f32>,
    append: Option<f32>,
}

impl TimeDebug{

    pub fn print(&self){
        println!("\t\t TO FLAT: find_index: {}ms", self.find_index.unwrap_or(1.0));
        println!("\t\t TO FLAT: setup_node: {}ms", self.setup_node.unwrap_or(1.0));
        println!("\t\t TO FLAT: setup_leaf: {}ms", self.setup_leaf.unwrap_or(1.0));
        println!("\t\t TO FLAT: count_size: {}ms", self.count_size.unwrap_or(1.0));
        println!("\t\t TO FLAT: clone: {}ms", self.clone.unwrap_or(1.0));
        println!("\t\t TO FLAT: append: {}ms", self.append.unwrap_or(1.0));
    }

    pub fn new() -> Self{
        TimeDebug{
            find_index: None,
            setup_node: None,
            setup_leaf: None,
            count_size: None,
            clone: None,
            append: None,
        }
    }

    fn add_find_index(&mut self, new: f32){
        if let Some(ref mut find_index) = self.find_index{
            *find_index += new;
            *find_index /= 2.0;
        }else{
            self.find_index = Some(new);
        }
    }

    fn add_setup_node(&mut self, new: f32){
        if let Some(ref mut setup_node) = self.setup_node{
            *setup_node += new;
            *setup_node /= 2.0;
        }else{
            self.setup_node = Some(new);
        }
    }

    fn add_setup_leaf(&mut self, new: f32){
        if let Some(ref mut setup_leaf) = self.setup_leaf{
            *setup_leaf += new;
            *setup_leaf /= 2.0;
        }else{
            self.setup_leaf = Some(new);
        }
    }

    pub fn add_count_size(&mut self, new: f32){
        if let Some(ref mut count_size) = self.count_size{
            *count_size += new;
            *count_size /= 2.0;
        }else{
            self.count_size = Some(new);
        }
    }

    pub fn add_clone(&mut self, new: f32){
        if let Some(ref mut clone) = self.clone{
            *clone += new;
            *clone /= 2.0;
        }else{
            self.clone = Some(new);
        }
    }

    pub fn add_append(&mut self, new: f32){
        if let Some(ref mut append) = self.append{
            *append += new;
            *append /= 2.0;
        }else{
            self.append = Some(new);
        }
    }
}

///Helper struct which collects data about a given mesh.
#[derive(Copy, Clone)]
pub struct MeshDataSize {
    pub bottom_bvh_buffer_len: usize,
    pub mesh_buffer_len: usize,
    pub vertex_buffer: usize,
    pub faces_buffer: usize,
}

impl MeshDataSize{
    pub fn append(&mut self, other: &MeshDataSize){
        self.bottom_bvh_buffer_len += other.bottom_bvh_buffer_len;
        self.mesh_buffer_len += other.mesh_buffer_len;
        self.faces_buffer += other.faces_buffer;
        self.vertex_buffer += other.vertex_buffer;
    }
}

///A things that implement this trait can be passed as a collection. The returning BVH will reference the right
/// index in its leafs
pub trait BvhBuildable {
    ///Returns the center of that object. Should usually be the center of its bound.
    fn get_center<T: BvhBuildable>(&self, buffer: &Vec<T>) -> Point3<f32>;
    fn get_aabb<T: BvhBuildable>(&self, buffer: &Vec<T>) -> Aabb3<f32>;
    ///Returns the sah value which typicly is `surface_area * polycount` for meshes.
    fn get_sah<T: BvhBuildable>(&self, buffer: &Vec<T>) -> f32;
    fn get_triangle_count<T: BvhBuildable>(&self, buffer: &Vec<T>) -> u32;
    fn get_mesh_data<T: BvhBuildable>(&self, buffer: &Vec<T>) -> MeshDataSize;
}

pub enum BvhNode {
    Node{
        aabb: Aabb3<f32>,
        left: Option<Arc<BvhNode>>,
        right: Option<Arc<BvhNode>>,
        ///The cost of this node ade from surface_area * num_triangles
        sah: f32,
        ///Count of triangles which are kept below this node
        tri_count: u32,
        idx: i32, //Will be set when indexing this tree
    },
    Leaf{
        aabb: Aabb3<f32>,
        buffer_idx: u32, //Pointer into the buffer
        idx: i32, //Will be set when indexing this tree
        //Describes
        mesh_data: MeshDataSize
    }
}

impl BvhNode{

    fn get_index(&self) -> i32{
        match self{
            BvhNode::Node{aabb,left,right,idx,sah,tri_count} => *idx,
            BvhNode::Leaf{aabb,buffer_idx,idx, mesh_data} => *idx,
        }
    }

    fn index_tree(&self, current_index: &mut i32) -> Arc<BvhNode>{
        //Takes this node, copy its content to a new node but with the right index.
        let this_index = current_index.clone();
        //advance for the next nodes
        *current_index += 1;
        match self{
            BvhNode::Node{aabb,left,right,idx,sah, tri_count} => {
                //Get the correct left and right tree, then setup self
                let left_new = if let Some(node) = left{
                    Some(node.index_tree(current_index))
                }else{
                    //has no node
                    None
                };
                let right_new = if let Some(node) = right{
                    Some(node.index_tree(current_index))
                }else{
                    None
                };

                Arc::new(
                    BvhNode::Node{
                        aabb: *aabb,
                        left: left_new,
                        right: right_new,
                        sah: *sah,
                        tri_count: *tri_count,
                        idx: this_index //The right index this time
                    }
                )

            },
            //Contains only the leave mesh
            BvhNode::Leaf{buffer_idx,aabb,idx: _, mesh_data} => {
                //Well thats easy, just take the index and return
                Arc::new(
                    BvhNode::Leaf{
                        buffer_idx: *buffer_idx,
                        aabb: *aabb,
                        idx: this_index,
                        mesh_data: *mesh_data,
                    }
                )
            }
        }
    }


    ///Build a TopBvh for the gpu based on the presented buffer collections and its parent as well
    /// as the presented collection of mesh_nodes (which provide worldspace info like material and transform info)
    /// which are referenced by this nodes `buffer_idx`.
    pub fn to_flat_top(
        &self,
        parent: Option<&ray_tracing::ty::BvhNode>,
        mesh_buffer: &mut Vec<ray_tracing::ty::Mesh>,
        bottom_bvh_buffer: &mut SubVec<ray_tracing::ty::BvhNode>,
        faces_buffer: &mut SubVec<ray_tracing::ty::Face>,
        vertex_buffer: &mut SubVec<ray_tracing::ty::Vertex>,
        mesh_nodes: &Vec<(Matrix4<f32>, Arc<RwLock<mesh::Mesh>>)>,
        //debug_data: &mut TimeDebug,
    ) -> Vec<ray_tracing::ty::BvhNode>{
        //build self
        match self{
            BvhNode::Node{aabb, left, right, idx, sah, tri_count} => {
                let left_idx = if let Some(node) = left{
                    node.get_index()
                }else{
                    -2 //no node
                };

                let right_idx = if let Some(node) = right{
                    node.get_index()
                }else{
                    -2 //no node
                };

                let esc_idx = {
                    if *idx == 0{
                        -1 //root has no escape node
                    }else{
                        //Test if we are the left one of our parent, if there is no parent we are also
                        //the root node in which case we can just set -1
                        if let Some(parent_node) = parent{
                            if parent_node.left == *idx{ //we are the left one
                                if parent_node.right == -2 {
                                    //use esc instead
                                    parent_node.esc_idx
                                }else{
                                    parent_node.right //our escape index
                                }
                            }else{
                                //we are not the left one so we have to be the right one...
                                parent_node.esc_idx //this is also ours
                            }
                        }else{
                            println!("WARNING no parent but we are not root...", );
                            -1 //we have no parent
                        }
                    }
                };

                let this_node = ray_tracing::ty::BvhNode{
                    aabb_min: aabb.min.to_vec().extend(1.0).into(),
                    aabb_max: aabb.max.to_vec().extend(1.0).into(),
                    left: left_idx,
                    right: right_idx,
                    esc_idx: esc_idx,
                    idx: *idx, //my idx
                    isLeaf: 0, //no
                    _pad0: 0,
                    _pad1: 0,
                    _pad2: 0,
                };

                //Since we know out selfs, lefts create the lower levels, and append
                let mut left_levels = if let Some(left_node) = left {
                    left_node.to_flat_top(
                        Some(&this_node),
                        mesh_buffer,
                        bottom_bvh_buffer,
                        faces_buffer,
                        vertex_buffer,
                        mesh_nodes,
                        //debug_data
                    )
                }else { Vec::new() };

                let mut right_levels = if let Some(right_node) = right {
                    right_node.to_flat_top(
                        Some(&this_node),
                        mesh_buffer,
                        bottom_bvh_buffer,
                        faces_buffer,
                        vertex_buffer,
                        mesh_nodes,
                        //debug_data,
                    )
                }else { Vec::new() };
                left_levels.append(&mut right_levels);
                left_levels.push(this_node);
                left_levels
            },

            BvhNode::Leaf{aabb,buffer_idx,idx, mesh_data: _} => {
                let esc_idx = {
                    if *idx == 0{
                        -1 //root has no escape node
                    }else{
                        //Test if we are the left one of our parent, if there is no parent we are also
                        //the root node in which case we can just set -1
                        if let Some(parent_node) = parent{
                            if parent_node.left == *idx{ //we are the left one
                                if parent_node.right == -2 {
                                    //use esc instead
                                    parent_node.esc_idx
                                }else{
                                    parent_node.right //our escape index
                                }
                            }else{
                                //we are not the left one so we have to be the right one...
                                parent_node.esc_idx //this is also ours
                            }
                        }else{
                            println!("WARNING no parent but we are not root...", );
                            -1 //we have no parent
                        }
                    }
                };

                let transform = mesh_nodes[*buffer_idx as usize].0.clone();

                let mesh_idx = mesh_buffer.len();
                //Since we are a leave node, update vertex buffer and push start and length into leaf
                mesh_nodes[*buffer_idx as usize].1.read().expect("failed to lock mesh")
                .as_ray_vertex_buffer(
                    transform,
                    mesh_buffer,
                    bottom_bvh_buffer,
                    faces_buffer,
                    vertex_buffer,
                    //debug_data,
                );

                vec![ray_tracing::ty::BvhNode{
                    aabb_min: aabb.min.to_vec().extend(1.0).into(),
                    aabb_max: aabb.max.to_vec().extend(1.0).into(),
                    left: mesh_idx as i32, //points to model in the mesh buffer
                    right: 0, //not used
                    esc_idx: esc_idx,
                    idx: *idx, //my idx
                    isLeaf: 1, //yes
                    _pad0: 0,
                    _pad1: 0,
                    _pad2: 0,
                }]
            }
        }
    }


    ///Builds a flat bvh for a set of faces withing a vertex buffer.
    pub fn to_flat_bottom(&self,
        parent: Option<&ray_tracing::ty::BvhNode>,
    ) -> Vec<ray_tracing::ty::BvhNode>{
        //build self
        match self{
            BvhNode::Node{aabb, left, right, idx, sah, tri_count} => {
                let left_idx = if let Some(node) = left{
                    node.get_index()
                }else{
                    -2 //no node
                };

                let right_idx = if let Some(node) = right{
                    node.get_index()
                }else{
                    -2 //no node
                };

                let esc_idx = {
                    if *idx == 0{
                        -1 //root has no escape node
                    }else{
                        //Test if we are the left one of our parent, if there is no parent we are also
                        //the root node in which case we can just set -1
                        if let Some(parent_node) = parent{
                            if parent_node.left == *idx{ //we are the left one
                                if parent_node.right == -2 {
                                    //use esc instead
                                    parent_node.esc_idx
                                }else{
                                    parent_node.right //our escape index
                                }
                            }else{
                                //we are not the left one so we have to be the right one...
                                parent_node.esc_idx //this is also ours
                            }
                        }else{
                            println!("WARNING no parent but we are not root...", );
                            -1 //we have no parent
                        }
                    }
                };


                let this_node = ray_tracing::ty::BvhNode{
                    aabb_min: aabb.min.to_vec().extend(1.0).into(),
                    aabb_max: aabb.max.to_vec().extend(1.0).into(),
                    left: left_idx,
                    right: right_idx,
                    esc_idx: esc_idx,
                    idx: *idx, //my idx
                    isLeaf: 0, //no
                    _pad0: 0,
                    _pad1: 0,
                    _pad2: 0,
                };

                //Since we know out selfs, lefts create the lower levels, and append
                let mut left_levels = if let Some(left_node) = left {
                    left_node.to_flat_bottom(
                        Some(&this_node),
                    )
                }else { Vec::new() };

                let mut right_levels = if let Some(right_node) = right {
                    right_node.to_flat_bottom(
                        Some(&this_node),
                    )
                }else { Vec::new() };
                left_levels.append(&mut right_levels);
                left_levels.push(this_node);
                left_levels
            },

            BvhNode::Leaf{aabb,buffer_idx,idx, mesh_data: _} => {
                let esc_idx = {
                    if *idx == 0{
                        -1 //root has no escape node
                    }else{
                        //Test if we are the left one of our parent, if there is no parent we are also
                        //the root node in which case we can just set -1
                        if let Some(parent_node) = parent{
                            if parent_node.left == *idx{ //we are the left one
                                if parent_node.right == -2 {
                                    //use esc instead
                                    parent_node.esc_idx
                                }else{
                                    parent_node.right //our escape index
                                }
                            }else{
                                //we are not the left one so we have to be the right one...
                                parent_node.esc_idx //this is also ours
                            }
                        }else{
                            println!("WARNING no parent but we are not root...", );
                            -1 //we have no parent
                        }
                    }
                };

                vec![ray_tracing::ty::BvhNode{
                    aabb_min: aabb.min.to_vec().extend(1.0).into(),
                    aabb_max: aabb.max.to_vec().extend(1.0).into(),
                    left: *buffer_idx as i32, //points to the face this node represents in the faces buffer
                    right: 0, //Not used
                    esc_idx: esc_idx,
                    idx: *idx, //my idx
                    isLeaf: 1, //yes
                    _pad0: 0,
                    _pad1: 0,
                    _pad2: 0,
                }]
            }
        }
    }

    //Collects all mesh data from this tree
    pub fn to_mesh_data(&self, other_data: &mut MeshDataSize){
        //Add self if we are a mesh else just travers
        match self{
            BvhNode::Node{aabb: _,left,right,idx: _, sah: _, tri_count: _} =>{
                if let Some(node) = left{
                    node.to_mesh_data(other_data);
                }
                if let Some(node) = right{
                    node.to_mesh_data(other_data);
                }
            }
            BvhNode::Leaf{aabb: _,buffer_idx: _, idx: _, mesh_data} =>{
                other_data.append(mesh_data);
            }
        }
    }

    pub fn print_tree(&self, lvl: u32){
        match self{
            BvhNode::Node{aabb,left,right,idx, sah, tri_count} => {
                ident(lvl);
                println!("NODE: {}", sah);
                if let Some(node) = left{
                    node.print_tree(lvl + 1);
                }
                if let Some(node) = right{
                    node.print_tree(lvl + 1);
                }
            },
            BvhNode::Leaf{aabb,buffer_idx, idx, mesh_data: _} => {
                ident(lvl);
                println!("LEAVE: {}", aabb.surface_area());
            },
        }
    }

    pub fn draw_bound(
        &self,
        lvl: u32,
        command_buffer: AutoCommandBufferBuilder,
        pipeline_manager: Arc<RwLock<PipelineManager>>,
        device: Arc<Device>,
        uniform_manager: Arc<RwLock<UniformManager>>,
        dynamic_state: &DynamicState,
        transform: Option<Matrix4<f32>>,
    ) -> AutoCommandBufferBuilder{

        let mut local_cb = command_buffer;

        //first draw bound of children if there are any
        let mut aabb = match self{
            BvhNode::Node{aabb,left,right,idx, sah, tri_count} => {
                if let Some(node) = left{
                    local_cb = node.draw_bound(
                        lvl + 1,
                        local_cb,
                        pipeline_manager.clone(),
                        device.clone(),
                        uniform_manager.clone(),
                        dynamic_state,
                        transform
                    );
                }

                if let Some(node) = right{
                    local_cb = node.draw_bound(
                        lvl + 1,
                        local_cb,
                        pipeline_manager.clone(),
                        device.clone(),
                        uniform_manager.clone(),
                        dynamic_state,
                        transform
                    );
                }

                //ret aabb
                aabb.clone()
            },
            BvhNode::Leaf{aabb,buffer_idx,idx,mesh_data} => {
                aabb.clone()
            },
        };

        //Get out own color
        let mut col = [1.0 / lvl as f32, 1.0, 1.0, 1.0];
        //Probably transform
        if let Some(trans) = transform{
            let new_aabb = Aabb3::new(
                trans.transform_point(aabb.min.clone()),
                trans.transform_point(aabb.max.clone())
            );
            aabb = new_aabb;
            //Also override color to see difference between mesh candidates
            col = [1.0 / lvl as f32, 1.0 / lvl as f32, 0.0, 1.0];
        }

        render_helper::draw_bound(
            local_cb,
            pipeline_manager,
            aabb, //the one we got from checking our self1
            device,
            uniform_manager,
            dynamic_state,
            col
        )
    }
}

impl BvhBuildable for BvhNode{
    fn get_center<T: BvhBuildable>(&self, buffer: &Vec<T>) -> Point3<f32>{
        match self{
            BvhNode::Node{aabb, left: _, right: _, idx: _, sah: _, tri_count: _} => aabb.center(),
            BvhNode::Leaf{aabb, buffer_idx: _, idx: _, mesh_data: _} => aabb.center(),
        }
    }
    fn get_aabb<T: BvhBuildable>(&self, buffer: &Vec<T>) -> Aabb3<f32>{
        match self{
            BvhNode::Node{aabb, left: _, right: _, idx: _, sah: _, tri_count: _} => aabb.clone(),
            BvhNode::Leaf{aabb, buffer_idx: _, idx: _, mesh_data: _} => aabb.clone(),
        }
    }

    ///TODO save sah dont do depth first search
    fn get_sah<T: BvhBuildable>(&self, buffer: &Vec<T>) -> f32{
        match self{
            BvhNode::Node{aabb: _, left: _, right: _, idx: _, sah, tri_count:_ } => {
                *sah
            },
            BvhNode::Leaf{aabb: _, buffer_idx, idx: _, mesh_data: _} => buffer[*buffer_idx as usize].get_sah(buffer),
        }
    }

    fn get_triangle_count<T: BvhBuildable>(&self, buffer: &Vec<T>) -> u32{
        match self{
            BvhNode::Node{aabb: _, left, right, idx: _, sah: _, tri_count} => {
                *tri_count
            },
            BvhNode::Leaf{aabb: _, buffer_idx, idx: _, mesh_data: _} => buffer[*buffer_idx as usize].get_triangle_count(buffer),
        }
    }

    fn get_mesh_data<T: BvhBuildable>(&self, buffer: &Vec<T>) -> MeshDataSize{
        match self{
            BvhNode::Node{aabb: _, left, right, idx: _, sah: _, tri_count} => {
                MeshDataSize{
                    bottom_bvh_buffer_len: 0,
                    mesh_buffer_len: 0,
                    faces_buffer: 0,
                    vertex_buffer: 0,
                }
            },
            BvhNode::Leaf{aabb: _, buffer_idx: _, idx: _, mesh_data} => *mesh_data,
        }


    }
}

///Builds a tree from the items
pub fn build_tree<T: BvhBuildable>(buffer: &Vec<T>) -> Arc<BvhNode>{
    //First, order the items in the buffer by morton code
    let mut morton_per_obj: Vec<(u128, usize)> = buffer.iter().enumerate()
    .map(|(idx,node)|{
        //For each idx, generate the morton code then return the morton code and the idx pointing into
        //The buffer
        (unbound_morton(node.get_center(buffer)), idx)
    }).collect();

    //Since we got the morton code we can now sort the whole thing by morton code
    morton_per_obj.as_mut_slice().sort_unstable_by(
        |(morton1, _node1), (morton2, _node2)| morton1.cmp(morton2)
    );

    //Collect all the genneric nodes into specific node leafs
    let mut leafs = Vec::new();
    for (_morton, node_idx) in morton_per_obj.iter(){
        leafs.push(Arc::new(BvhNode::Leaf{
            aabb: buffer[*node_idx].get_aabb(buffer),
            buffer_idx: *node_idx as u32, //Pointer into the buffer
            idx: 0, //Set later,
            mesh_data: buffer[*node_idx].get_mesh_data(buffer),
        }));
    }
    let inst = Instant::now();
    let leaf_length = leafs.len();
    //Now build the tree hierachy based on the morton location and splited at the sah mediant
    let mut tree = build_sah_level(buffer, leafs).expect("Failed to build bvh hierachy");

    //index tree
    let mut current_idx = 0;
    tree = tree.index_tree(&mut current_idx);
    tree
}

fn build_sah_level<T: BvhBuildable>(buffer: &Vec<T>, mut nodes: Vec<Arc<BvhNode>>) -> Option<Arc<BvhNode>>{
    //Are we a child node with only one bvh node?
    if nodes.len() == 1{
        return Some(nodes.pop().expect("Could not pop bvh node despite being of length 1"));
    }
    //If we are smaller then 1, we started with no mesh at all, return a empty node
    if nodes.len() < 1{
        return Some(Arc::new(BvhNode::Node{
            left: None,
            right: None,
            aabb: Aabb3::new(Point3::new(0.0,0.0,0.0),Point3::new(0.0,0.0,0.0)),
            sah: 1.0,
            tri_count: 1,
            idx: -2
        }));
    }

    //no?
    //Lets find where we have to split our vec to get two equaly big surfaces

    //Now find index to split so that
    // Cost(l) + Cost(r) becomes minimal.
    //TODO find a actual faster sah algo

    //Since our nodes are ordered and we currently don't care about their size, we just split the
    //Array when we overtake the wholecost/2
    let whole_cost = nodes.iter().fold(0.0, |acc, node| acc + node.get_sah(buffer));
    //now find the right split index
    let mut current_cost = 0.0;
    let mut split_index = 0;
    for node in nodes.iter(){
        if current_cost > whole_cost / 2.0{
            break; //Found nice index before
        }

        current_cost += node.get_sah(buffer);
        split_index += 1;
    }
    //Correct to the one before since it could happen that only the last on is used or the 0 has to be used
    if split_index > 1{
        split_index = split_index - 1;
    }

    let right = nodes.split_off(split_index);

    //Now since we have left and right, let them create the final bvh node and assemble
    let left_node = build_sah_level(buffer, nodes);
    let right_node = build_sah_level(buffer, right);
    let this_aabb = if let Some(ref ln) = left_node{
        if let Some(ref rn) = right_node{
            ln.get_aabb(buffer).union(&rn.get_aabb(buffer))
        }else{
            ln.get_aabb(buffer).clone()
        }
    }else{
        println!("THERE WAS A PROBLEM WITH AABB Construction!", );
        Aabb3::new(Point3::new(0.0,0.0,0.0),Point3::new(0.0,0.0,0.0))
    };

    //Find the sah of this node
    let mut new_tri_count = if let Some(ref node) = left_node{
        node.get_triangle_count(buffer)
    }else{
        0
    };

    if let Some(ref node) = right_node{
        new_tri_count += node.get_triangle_count(buffer);
    };
    let this_sah = this_aabb.surface_area() * new_tri_count as f32;

    Some(Arc::new(BvhNode::Node{
        left: left_node,
        right: right_node,
        aabb: this_aabb,
        sah: this_sah,
        tri_count: new_tri_count,
        idx: 0, //Will be set later when indexing
    }))
}

///Take a 32bit uint and expands it to an 128bit uint where each original has two zeros till the next one.
/// Example: original: 1234 -> _ _ 1 _ _ 2 _ _ 3 _ _ 4
// taken from [here](https://stackoverflow.com/questions/1024754/how-to-compute-a-3d-morton-number-interleave-the-bits-of-3-ints#1024889)
pub fn expand_bits_128(mut v: u32) -> u128{
    v =  v ^ ( 1 << 31 );
    let mut x = v as u128;
    //Magic bit shifting... ending with x_ _ x _ _ x _ _ .. where each x is a original bit and each
    // _ is a zero
    x &= 0x3ffffffffff;
    x = (x | x << 64) & 0x3ff0000000000000000ffffffffu128;
    x = (x | x << 32) & 0x3ff00000000ffff00000000ffffu128;
    x = (x | x << 16) & 0x30000ff0000ff0000ff0000ff0000ffu128;
    x = (x | x << 8) & 0x300f00f00f00f00f00f00f00f00f00fu128;
    x = (x | x << 4) & 0x30c30c30c30c30c30c30c30c30c30c3u128;
    x = (x | x << 2) & 0x9249249249249249249249249249249u128;
    x
}

///takes a set of floating points as a 3d point in space and calculates the morton code from them in 128bit
// (actually 32*3 bit are used). The point can be within any region.
pub fn unbound_morton(point: Point3<f32>) -> u128{
    //shift each axis so have a gap of two zeros between each bit
    let xx = expand_bits_128(point.x.to_bits());
    let yy = expand_bits_128(point.y.to_bits());
    let zz = expand_bits_128(point.z.to_bits());
    //now assemble
    return xx | (yy << 1) | (zz << 2);
}

pub fn ident(lvl: u32){
    for id in 0..lvl{
        print!("---", );
    }
}
