
extern crate vulkano;
extern crate jakar_engine;
extern crate cgmath;
extern crate jakar_tree;

use cgmath::*;

use jakar_engine::*;
use jakar_engine::core::next_tree::*;
use jakar_engine::core::resources::*;
use jakar_engine::core::resources::camera::Camera;
use jakar_engine::core::resources::light;
use jakar_engine::core::next_tree::*;
use jakar_tree::node::*;
use jakar_engine::core::render_settings::*;
use jakar_engine::core::next_tree::JakarNode;
use jakar_engine::core::next_tree::jobs::SceneJobs;

use jakar_engine::rand::{thread_rng, Rng};

use std::thread;
use std::sync::{Arc, RwLock};
use std::time::{Instant, Duration};

extern crate winit;

fn main() {

    let light_settings = LightSettings::new(ShadowSettings::new(
        1, //pass_resolution
        4, //max_casting_lights
    ));

    let graphics_settings = core::render_settings::RenderSettings::default()
    .with_msaa_factor(8)
    .with_gamma(1.5)
    .with_light_settings(light_settings)
    .with_vsync(false)
    .with_bloom(
        8, //levles
        2, //initial blured level
        1.5, //size
        0.02, //brightness
    ).with_ssao(
        23, //sample_count
        1.5, //radius
        1.5 //intensity
    );


    let settings = core::engine_settings::EngineSettings::default()
    .with_dimensions(1600, 900)
    .with_name("Jakar Instance")
    .in_release_mode()
    .with_max_input_polling_speed(200)
    .with_fullscreen_mode(false)
    .with_cursor_state(core::engine_settings::CursorState::Default)
    //.with_cursor_state(core::engine_settings::CursorState::GrabAndHide)
    //.with_cursor_visibility(winit::MouseCursor::Default)
    .with_cursor_visibility(winit::MouseCursor::Arrow)
    .with_render_settings(graphics_settings)
    .with_camera_settings(core::engine_settings::CameraSettings{
        far_plane: 1000.0,
        near_plane: 0.2,
        use_auto_exposure: false,
        sensitivity: 80,
        aperture: 2.0,
        shutter_speed: 1.0 / 50.0,
    });

    //Start the engine
    let mut engine = match jakar_engine::JakarEngine::build(Some(settings)){
        Ok(eng) => eng,
        Err(_) => {
            println!("Failed to create engine!");
            return;
        }
    };


    //engine.get_asset_manager_mut().import_gltf("TestScene", "examples/simple_scene/TestScenes/Cube_Plane.gltf");
    //engine.get_asset_manager_mut().import_gltf("TestScene", "examples/simple_scene/pica_pica/scene.gltf");
    //engine.get_asset_manager_mut().import_gltf("TestScene", "examples/simple_scene/GLBTest/test3.gltf");
    engine.get_asset_manager_mut().import_gltf("TestScene", "examples/simple_scene/Gate/Gate.gltf");
    //engine.get_asset_manager_mut().import_gltf("TestScene", "examples/simple_scene/Sponza/Sponza.gltf");
    //engine.get_asset_manager_mut().import_gltf("TestScene", "examples/simple_scene/Helmet/Helmet.gltf");

    engine.get_asset_manager_mut().import_gltf("Other_Scene", "examples/simple_scene/GLBTest/test3.gltf");

    let mut light_tree =jakar_tree::tree::Tree::new(
        jakar_engine::core::next_tree::content::ContentType::Empty(core::resources::empty::Empty::new("LightsRoot")),
        jakar_engine::core::next_tree::attributes::NodeAttributes::default()
    );

    //SUN========================================================================
    //add a matrix of lights
    let mut light_count = 3;
    let interval = [-25.0, 25.0];

    let mut rng = thread_rng();
    for _ in 0..light_count{
        //Find random location
        let x: f32 = rng.gen_range(interval[0], interval[1]);
        let y: f32 = rng.gen_range(interval[0], interval[1]);

        let mut point = light::LightPoint::new("LightPoint");
        point.set_intensity(
            1600.0
        );
        point.set_color(
            Vector3::new(
                1.0, //(x + matrix_size) as f32 / matrix_size as f32,
                1.0, //(y + matrix_size) as f32 / matrix_size as f32,
                1.0, //(1-y + matrix_size) as f32 / matrix_size as f32
            )
        );
        point.set_radius(20.0);

        let node_name = light_tree
        .add_at_root(content::ContentType::PointLight(point), None, None).expect("Failed to add light node");

        //Set the location
        match light_tree.get_node(&node_name){
            Some(scene) => {
                scene.add_job(
                    jobs::SceneJobs::Move(
                        Vector3::new(
                            x,
                            2.0,
                            y,
                        )
                    )
                );
                scene.add_job(
                    jobs::SceneJobs::Rotate(
                        Vector3::new(
                            10.0,
                            0.0,
                            0.0,
                        )
                    )
                );

                let mut scene_controller = core::next_tree::node_controller::ctrl_random::RandomMovementController::default();
                scene_controller.set_interval(
                    [Point3::new(interval[0], 2.0, interval[0]),
                    Point3::new(interval[1], 15.0, interval[1])]
                );
                scene_controller.set_speed(0.2);
                scene_controller.set_turn_intensity_and_foresee(0.1, 4.0);
                scene.set_controller(
                    scene_controller
                );
            }
            None => {println!("Could not find Light", );}, //get on with it
        }
    }

/*
    //Now add a sun
    let mut sun = light::LightDirectional::new("Sunny");
    sun.set_intensity(100_000.0);
    sun.set_color(Vector3::new(1.0, 0.85, 0.9));
    let sun_node = light_tree.add_at_root(content::ContentType::DirectionalLight(sun), None, None).expect("fail");
    //Now rotate it a bit on x
    match light_tree.get_node(&sun_node){
        Some(sun)=> {
            sun.add_job(jobs::SceneJobs::Rotate(Vector3::new(0.0, -10.0, -40.0)));

        },
        None => {println!("Could not find sun", );}
    }
*/

    light_tree.update();
    engine.get_asset_manager_mut().get_active_scene().join_at_root(&light_tree);
    println!("LightreeJoined!", );
    engine.get_asset_manager_mut().get_active_scene().update();
    //println!("THE SCENE ==================================================", );
    //engine.get_asset_manager().get_active_scene().print_tree();
    //println!("END ========================================================", );
    let mut scene_added = false;
    let mut scene_scaled = false;
    //Its time to actually start the engine
    engine.start();

    'game_loop: loop{

        //try adding by brute force to the main scene, could be handled nice :D
        if !scene_added{
            //println!("Adding Test Scene to main scnene", );
            match engine.get_asset_manager_mut().activate_scene("TestScene"){
                Ok(_) => scene_added = true,
                Err(_) => {
                    //println!("Could not find TestScene", );
                }
            }
        }
/*
        match engine.get_asset_manager_mut().get_active_scene().get_node(&sun_node){
            Some(sun)=> {
                sun.add_job(jobs::SceneJobs::Rotate(Vector3::new(0.0, 0.0, 0.2)));
            },
            None => {println!("Could not find sun", );}
        }
*/

        //test if a is pressed
        if engine.get_asset_manager().get_keymap().h{

            match engine.get_asset_manager_mut().get_active_scene().get_node("TestScene"){
                Some(scene) => {
                    scene.add_job(jobs::SceneJobs::Rotate(Vector3::new(0.0, 1.0, 0.0)));
                }
                None => {println!("Could not find TestScene", );}, //get on with it
            }
        }

        if engine.get_asset_manager().get_keymap().g{

            match engine.get_asset_manager_mut().get_active_scene().get_node("TestScene"){
                Some(scene) => {
                    scene.add_job(jobs::SceneJobs::Move(Vector3::new(0.0, 0.0, 0.001)));
                }
                None => {println!("Could not find TestScene", );}, //get on with it
            }
        }

        if engine.get_current_keymap().p{
            engine.get_settings_mut().capture_next_frame();
        }

        if engine.get_current_keymap().r{
            engine.get_asset_manager_mut().get_active_scene().print_tree();
        }

        if engine.get_current_keymap().up{
            engine.get_settings_mut().
            get_render_settings_mut().get_debug_settings_mut().ldr_debug_view_level += 1;
        }


        if engine.get_current_keymap().down{
            engine.get_settings_mut()
            .get_render_settings_mut().get_debug_settings_mut().ldr_debug_view_level -= 1;
        }

        if engine.get_current_keymap().f1{
            engine.get_settings_mut()
            .get_render_settings_mut().get_debug_settings_mut().ldr_debug_view_level = 0;
        }
        if engine.get_current_keymap().f2{
            engine.get_settings_mut()
            .get_render_settings_mut().get_debug_settings_mut().ldr_debug_view_level = 1;
        }
        if engine.get_current_keymap().f3{
            engine.get_settings_mut()
            .get_render_settings_mut().get_debug_settings_mut().ldr_debug_view_level = 2;
        }
        if engine.get_current_keymap().f4{
            engine.get_settings_mut()
            .get_render_settings_mut().get_debug_settings_mut().ldr_debug_view_level = 3;
        }
        if engine.get_current_keymap().f5{
            engine.get_settings_mut()
            .get_render_settings_mut().get_debug_settings_mut().ldr_debug_view_level = 4;
        }
        if engine.get_current_keymap().f6{
            engine.get_settings_mut()
            .get_render_settings_mut().get_debug_settings_mut().ldr_debug_view_level = 5;
        }
        if engine.get_current_keymap().f7{
            engine.get_settings_mut()
            .get_render_settings_mut().get_debug_settings_mut().ldr_debug_view_level = 6;
        }
        if engine.get_current_keymap().f8{
            engine.get_settings_mut()
            .get_render_settings_mut().get_debug_settings_mut().ldr_debug_view_level = 7;
        }
        if engine.get_current_keymap().f9{
            engine.get_settings_mut()
            .get_render_settings_mut().get_debug_settings_mut().ldr_debug_view_level = 8;
        }
        if engine.get_current_keymap().f10{
            engine.get_settings_mut()
            .get_render_settings_mut().get_debug_settings_mut().ldr_debug_view_level = 9;
        }
        if engine.get_current_keymap().f11{
            engine.get_settings_mut()
            .get_render_settings_mut().get_debug_settings_mut().ldr_debug_view_level = 10;
        }
        if engine.get_current_keymap().f12{
            engine.get_settings_mut()
            .get_render_settings_mut().get_debug_settings_mut().ldr_debug_view_level = 11;
        }
        //Set the debug settings
        if engine.get_current_keymap().b{
            engine.get_settings_mut().get_render_settings_mut()
            .get_bloom_mut().brightness += 0.001;
            println!("Changed stuff", );
        }
        //Set the debug settings
        if engine.get_current_keymap().n{
            engine.get_settings_mut().get_render_settings_mut()
            .get_bloom_mut().brightness -= 0.001;
            println!("Changed stuff", );
        }

        if engine.get_current_keymap().t_1{
            engine.get_settings_mut().get_render_settings_mut()
            .get_debug_settings_mut().debug_view = jakar_engine::core::render_settings::DebugView::MainDepth;
        }

        if engine.get_current_keymap().t_2{
            engine.get_settings_mut().get_render_settings_mut()
            .get_debug_settings_mut().debug_view = jakar_engine::core::render_settings::DebugView::DebugGrid;
        }

        if engine.get_current_keymap().t_3{
            engine.get_settings_mut().get_render_settings_mut()
            .get_debug_settings_mut().debug_view = jakar_engine::core::render_settings::DebugView::ShadowMaps;
        }

        if engine.get_current_keymap().t_4{
            engine.get_settings_mut().get_render_settings_mut()
            .get_debug_settings_mut().debug_view = jakar_engine::core::render_settings::DebugView::DirectionalDepth;
        }

        if engine.get_current_keymap().t_5{
            engine.get_settings_mut().get_render_settings_mut()
            .get_debug_settings_mut().debug_view = jakar_engine::core::render_settings::DebugView::GBuffer;
        }

        if engine.get_current_keymap().t_6{
            engine.get_settings_mut().get_render_settings_mut()
            .get_debug_settings_mut().debug_view = jakar_engine::core::render_settings::DebugView::GBufferTwo;
        }

        if engine.get_current_keymap().t_7{
            engine.get_settings_mut().get_render_settings_mut()
            .get_debug_settings_mut().debug_view = jakar_engine::core::render_settings::DebugView::Shaded;
        }

        if engine.get_current_keymap().b{
            engine.get_settings_mut().get_render_settings_mut()
            .get_debug_settings_mut().draw_bounds = true;
        }

        if engine.get_current_keymap().n{
            engine.get_settings_mut().get_render_settings_mut()
            .get_debug_settings_mut().draw_bounds = false;
        }

        //test if a is pressed
        if engine.get_current_keymap().escape{
            engine.end();
            break;
        }

        thread::sleep(Duration::from_millis(10));

    }
}
