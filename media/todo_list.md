# Things currently to do

1. [x] make AABB's drawable
2. [x] remove the forward pipeline and build a deferred renderer (no transparency)
  1. [x] find a nice slim GBuffer
  2. [x] remove msaa
  3. [x] calculate diffuse/specular lighting
  4. [x] modify the mixing of both terms
3. [x] generate a BVH of all meshes (per frame?)
4. [x] build gpu buffer of BVH which is traversable in a compute shader
5. [x] test traversal on the gpu
6. [x] create shadow image in view space via compute shader
7. [x] find out how to blend with deferred image
8. [ ] find method to send all textures to compute shader
9. [ ] create perfect reflection image for each object
10. [ ] blend reflection based on shininess and metalness
11. [ ] assemble all the image stage to a nice final image


# things I "know"
Performance:

for one shadow ray per light and four reflection rays I'd need `[light_count+4] * [img_width * img_height]` rays per frame, which is a bit much I guess.
Idea: Do one shadow ray and one reflections ray. Do a second pass where we blur shadow based on distance to occluder (faked soft shadows)
and the same for the reflection for diffuse reflections. Problem: Big things in the level make the blur "incorrect".

Also, currently I only plan to build a bvh for the objects. Therefore high polycounts are
not possible. Maybe I'll have to build a second bvh per object which takes, if an object is hit, the
ray, transforms it into model space and does a second trace through the mesh, the finished result gets transformed back into
worldspace for the final result.

# Other TODO
[x] change all RwLock usges to RwLock which can read without locking, which is nice and should be faster.
